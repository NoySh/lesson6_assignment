﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace hw19_Noy_Shafferman
{
    public partial class Form2 : Form
    {
        string username;
        public Form2(string username)
        {
            this.username = username;
            InitializeComponent();
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {


        }

        private void monthCalendar1_DateSelected(object sender, DateRangeEventArgs e)
        {
           
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string date = "", newDate = "";
            string name = "", line, birthdate = "";
            string[] dateArr = new string[3];
            date = monthCalendar1.SelectionRange.Start.ToShortDateString();
            dateArr = date.Split('/');
            newDate = dateArr[1].Substring(1) + '/' + dateArr[0] + '/' + dateArr[2];
            var lines = File.ReadAllLines(username + "BD.txt");
            for (int i = 0; i < lines.Length; i += 1)
            {
                line = lines[i];
                name = line.Substring(0, line.IndexOf(','));
                birthdate = line.Substring(line.IndexOf(',') + 1);
                if (newDate.Equals(birthdate))
                {
                    label5.Text = "It's " + name + "'s birthday!";
                    label5.Font = new Font("Rockwell", 10, FontStyle.Bold,
                       GraphicsUnit.Point);
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            bool isTaken = false;
            string date = "", line, birthdate = "";
            string[] dateArr = new string[3];
            date = dateTimePicker2.Value.ToString("M/d/yyyy");
            var lines = File.ReadAllLines(username + "BD.txt");
            for (int i = 0; i < lines.Length; i += 1)
            {
                line = lines[i];
                birthdate = line.Substring(line.IndexOf(',') + 1);
                if (date.Equals(birthdate))
                {
                    MessageBox.Show("Birthdate is already taken.");
                    isTaken = true;
                }
            }
            if (isTaken == false)
            {
                TextWriter tsw = new StreamWriter(username + "BD.txt", true);
                tsw.WriteLine('\n'+name.Text + ',' + date);
                tsw.Close();
                label6.Text = "Saved";
                label6.Font = new Font("Rockwell", 10, FontStyle.Bold,
                   GraphicsUnit.Point);
            }

        }
    }
}